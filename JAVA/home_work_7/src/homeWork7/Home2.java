//Реализуйте метод рисующий на экране прямоугольник из звездочек «*» — его параметрами будут
//целые числа которые описывают длину и ширину такого прямоугольника.

package homeWork7;

public class Home2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		drawRectangle(5, 15);
	}
	public static void drawRectangle(int length, int width) {
	    for (int i = 0; i < length; i++) {
	        for (int j = 0; j < width; j++) {
	            System.out.print("*");
	        }
	        System.out.println();
	    }
	}
}
