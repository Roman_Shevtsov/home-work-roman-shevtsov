//Напишите метод который вернет максимальное число из массива целых чисел.


package homeWork7;

public class Home1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {1, 3, 5, 2, 4};
	    int max = findMax(arr);
	    System.out.println("Максимальний елемент масиву: " + max);
	}
	public static int findMax(int[] arr) {
	    int max = arr[0];
	    for (int i = 1; i < arr.length; i++) {
	        if (arr[i] > max) {
	            max = arr[i];
	        }
	    }
	    return max;
	}
}
