//Напишите метод который реализует линейный поиск элемента в массиве целых чисел. Если такой
//элемент в массиве есть то верните его индекс, если нет то метод должен возвращать число - «-1»

package homeWork7;

public class Home3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5};
	    int x = 9;
	    int index = linearSearch(arr, x);
	    if (index == -1) {
	        System.out.println("Елемент " + x + " не знайдено в масиві");
	    } else {
	        System.out.println("Елемент " + x + " знайдено в масиві на позиції " + index);
	    }
	    
	}
	public static int linearSearch(int[] arr, int x) {
	    for (int i = 0; i < arr.length; i++) {
	        if (arr[i] == x) {
	            return i;
	        }
	    }
	    return -1;
	}
}
