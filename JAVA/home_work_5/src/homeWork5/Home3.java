//Создать массив случайных чисел (размером 15 элементов). Создайте второй массив в два раза
//больше, первые 15 элементов должны быть равны элементам первого массива, а остальные
//элементы заполнить удвоенных значением начальных. Например:
//Было → {1,4,7,2}
//Стало → {1,4,7,2,2,8,14,4}

package homeWork5;

import java.util.Random;

public class Home3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 int[] arr1 = new int[15];
	        int[] arr2 = new int[30];

	        Random random = new Random();

	        for (int i = 0; i < arr1.length; i++) {
	            arr1[i] = random.nextInt(100);
	        }

	        for (int i = 0; i < arr1.length; i++) {
	            arr2[i] = arr1[i];
	            arr2[i + arr1.length] = arr1[i] * 2;
	        }

	        System.out.println("Перший масив: ");
	        for (int i = 0; i < arr1.length; i++) {
	            System.out.print(arr1[i] + " ");
	        }

	        System.out.println("\n\nДругий масив: ");
	        for (int i = 0; i < arr2.length; i++) {
	            System.out.print(arr2[i] + " ");
	        }
	}

}
