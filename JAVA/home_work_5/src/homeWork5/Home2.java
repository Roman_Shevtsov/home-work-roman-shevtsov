//Написать код для возможности создания массива целых чисел (размер вводиться с клавиатуры) и
//возможности заполнения каждого его элемента вручную. Выведите этот массив на экран.

package homeWork5;

import java.util.Scanner;

public class Home2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);

        System.out.print("Введіть розмір масиву: ");
        int size = scanner.nextInt();

        int[] arr = new int[size];

        for (int i = 0; i < size; i++) {
            System.out.print("Введіть елемент масиву " + (i + 1) + ": ");
            arr[i] = scanner.nextInt();
        }

        System.out.print("Масив: ");
        for (int i = 0; i < size; i++) {
            System.out.print(arr[i] + " ");
        }
	}

}
