//Дан массив целых чисел вида — {0,5,2,4,7,1,3,19}. Написать программу для подсчета количества
//нечетных чисел в нем.

package homeWork5;

public class Home1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {0, 5, 2, 4, 7, 1, 3, 19};
        int count = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                count++;
            }
        }

        System.out.println("Кількість непарних чисел: " + count);
	}

}
