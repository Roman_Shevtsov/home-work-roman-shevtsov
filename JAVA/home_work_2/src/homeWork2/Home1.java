//1)Написать программу которая вычислит и выведет на экран площадь треугольника если
//известны его стороны (sideA = 0.3, sideB = 0.4, sideC = 0.5). Для вычисления
//использовать формулу Герона.

package homeWork2;

public class Home1 {

	public static void main(String[] args) {
		double sideA = 0.3;
		double sideB = 0.4;
		double sideC = 0.5;
		double resultP;
		double resultS;
		
		resultP = (sideA + sideB + sideC) / 2;
		resultS = Math.sqrt(resultP * (resultP - sideA) * (resultP - sideB) * (resultP - sideC));
		
		System.out.println("площа трикутника складає: " + resultS);
		
	}

}
