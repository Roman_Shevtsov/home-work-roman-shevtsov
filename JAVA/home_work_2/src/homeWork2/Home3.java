//3)Один литр топлива стоит 1.2$. Ваш автомобиль тратит на 100 км пути 8 литров топлива.
//Вы собрались в поездку в соседний город. Расстояние до этого города составляет 120
//км. Вычислите и выведите на экран сколько вам нужно заплатить за топливо для
//поездки.

package homeWork2;

public class Home3 {

	public static void main(String[] args) {
		double costFuel = 1.2;
		double costKm = 8.00 / 100;
		double costRoad;
		int road = 120;
		
		costRoad = costKm * road * costFuel;
		
		System.out.println("Вартість поїздки: " + costRoad + " $");
	}

}
