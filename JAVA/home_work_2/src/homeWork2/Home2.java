//2)Стоимость яблока составляет 2$. Покупатель приобретает 6 яблок. Напишите
//программу которая вычислит и выведет на экран сумму которую должен уплатить
//покупатель за покупку.

package homeWork2;

public class Home2 {

	public static void main(String[] args) {
		int costAple = 2;
		int apple = 6;
		int sum;
		
		sum = costAple * apple;
		
		System.out.println("Покупець повинен оплатити: " + sum + "$ за " + apple + " яблук");
	}

}
