//Вводится строка из слов, разделенных пробелами. Найти самое длинное слово и вывести его на экран.

package homeWork6;

import java.util.Scanner;

public class Home2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);

        System.out.print("Введіть рядок тексту: ");
        String text = scanner.nextLine();

        String[] words = text.split(" ");

        String longestWord = "";

        for (String word : words) {
            if (word.length() > longestWord.length()) {
                longestWord = word;
            }
        }

        System.out.println("Найдовше слово: " + longestWord);
	}

}
