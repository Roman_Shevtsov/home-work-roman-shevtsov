//Считайте строку текста с клавиатуры. Подсчитайте сколько раз в нем встречается символ «b».

package homeWork6;

import java.util.Scanner;

public class Home1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);

        System.out.print("Введіть рядок тексту: ");
        String text = scanner.nextLine();

        int count = 0;

        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == 'b') {
                count++;
            }
        }

        System.out.println("Кількість входжень символу 'b': " + count);
	}

}
