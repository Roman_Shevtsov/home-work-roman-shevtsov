//1)Написать программу которая считает 4 целых числа с клавиатуры и выведет на экран самое
//большое из них.

package homeWork3;

import java.util.Scanner;

public class Home1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a;
		System.out.println("Введіть перше число");
		a = sc.nextInt();
		int b;
		System.out.println("Введіть друге число");
		b = sc.nextInt();
		int c;
		System.out.println("Введіть третє число");
		c = sc.nextInt();
		int d;
		System.out.println("Введіть четверте число");
		d = sc.nextInt();
		int max;
		max = a;
        if (b > max) {
        	max = b;
        }
        if (c > max) {
        	max = c;
        }
        if (d > max) {
        	max = d;
        }
        System.out.println("Максимальне введене число = " + max);
	}

}
