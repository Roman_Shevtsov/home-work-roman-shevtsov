//2) Есть девятиэтажный дом, в котором 5 подъездов. Номер подъезда начинается с единицы. На
//одном этаже 4 квартиры. Напишите программу которая, получит номер квартиры с клавиатуры, и
//выведет на экран, на каком этаже, какого подъезда расположена эта квартира. Если такой
//квартиры нет в этом доме, то нужно сообщить об этом пользователю.

package homeWork3;

import java.util.Scanner;

public class Home2 {

	public static void main(String[] args) {
		 Scanner sc = new Scanner(System.in);
		 
		 int ap;
			int entrance = 0;
			int flour = 0;
			
			System.out.println("Введіть номер квартири: ");
			ap = sc.nextInt();
			
			if (ap > 0 && ap < 145) {
			
				if(ap%36 != 0) {
					entrance = ap/36 + 1;
				} else {
					entrance = ap/36;
					}
				
				if((ap - ((entrance - 1) * 36))%4 != 0) {
					flour = (ap - ((entrance - 1) * 36))/4 + 1;
				} else {
					flour = (ap - ((entrance - 1) * 36))/4;
					}
				
				System.out.println("Це підїзд №: " + entrance);
				System.out.println("Поверх: " + flour);
			} else {
				System.out.println("Такої квартири немає!!!");
			}

	}

}
