//3) Треугольник существует только тогда, когда сумма любых двух его сторон больше третьей. Дано: a,
//b, c – стороны предполагаемого треугольника. Напишите программу, которая укажет, существует ли
//такой треугольник или нет.

package homeWork3;

import java.util.Scanner;

public class Home3 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

        System.out.print("Введіть довжину сторони a: ");
        double a = scanner.nextDouble();

        System.out.print("Введіть довжину сторони b: ");
        double b = scanner.nextDouble();

        System.out.print("Введіть довжину сторони c: ");
        double c = scanner.nextDouble();

        boolean triangleExists = a + b > c && a + c > b && b + c > a;

        if (triangleExists) {
            System.out.println("Такий трикутник існує.");
        } else {
            System.out.println("Такий трикутник не існує.");
        }
	}

}
