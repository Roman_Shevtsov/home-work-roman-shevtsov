//Реализуйте метод который выведет на экран список всех каталогов расположенных в каталоге
//адрес которого будет параметром этого метода.


package homeWork8;

import java.io.File;
import java.util.Scanner;

public class Home3 {

	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введіть шлях до каталогу:");
        String path = scanner.nextLine();

        listDirectories(path);
    }

    public static void listDirectories(String path) {
        File directory = new File(path);
        File[] directories = directory.listFiles(File::isDirectory);

        if (directories == null) {
            System.out.println("Каталог " + path + " не існує або не є каталогом");
        } else {
            System.out.println("Список каталогів, що знаходяться у " + path + ":");
            for (File dir : directories) {
                System.out.println(dir.getName());
            }
        }
    }

}
