//Создайте консольный «текстовый редактор» с возможностью сохранения набранного текста в файл.

package homeWork8;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Home1 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
        StringBuilder sb = new StringBuilder();

        System.out.println("Введіть текст. Для завершення введення введіть рядок 'end':");

        while (true) {
            String line = scanner.nextLine();
            if (line.equals("end")) {
                break;
            }
            sb.append(line);
            sb.append(System.lineSeparator());
        }

        System.out.println("Введіть ім'я файлу для збереження:");
        String filename = scanner.nextLine();

        try {
            FileWriter writer = new FileWriter(filename);
            writer.write(sb.toString());
            writer.close();
            System.out.println("Текст успішно збережено у файлі " + filename);
        } catch (IOException e) {
            System.err.println("Не вдалося зберегти текст у файлі " + filename);
            e.printStackTrace();
        }
    }
}