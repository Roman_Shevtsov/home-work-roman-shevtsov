//Напишите метод для сохранения в текстовый файл двухмерного массива целых чисел.

package homeWork8;

import java.io.FileWriter;
import java.io.IOException;

public class Home2 {

	public static void main(String[] args) {
		int[][] arr = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
	    ArrayToFile.saveArrayToFile(arr, "array.txt");
	}
	
	public class ArrayToFile {
	    public static void saveArrayToFile(int[][] arr, String filename) {
	        try {
	            FileWriter writer = new FileWriter(filename);
	            for (int i = 0; i < arr.length; i++) {
	                for (int j = 0; j < arr[i].length; j++) {
	                    writer.write(arr[i][j] + " ");
	                }
	                writer.write(System.lineSeparator());
	            }
	            writer.close();
	            System.out.println("Масив успішно збережено у файлі " + filename);
	        } catch (IOException e) {
	            System.err.println("Не вдалося зберегти масив у файлі " + filename);
	            e.printStackTrace();
	        }
	    }
	}

}
