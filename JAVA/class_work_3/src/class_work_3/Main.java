package class_work_2;

import java.util.Scanner;

public class main {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int coffePrice = 30;
    int croissantPrice = 70;
    int money;
    System.out.println("How much money do you have? ");
    money = sc.nextInt();

    if (money >= coffePrice) {
      System.out.println("You buy coffe");
      money = money - coffePrice;
      if (money >= croissantPrice) {
        System.out.println("You buy croissant");
        money = money - croissantPrice;
      }
    } else {
      System.out.println("Bad day");
    }

    System.out.println("Money = " + money);
  }
}