//Напечатайте таблицу умножения на 5. Предпочтительно печатать 1 x 5 = 5, 2 x 5 = 10, а не просто 5, 10 и т. д.

package homeWork4;

public class Home2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 5;

        for (int i = 1; i <= 10; i++) {
            int product = num * i;
            System.out.println(i + " x " + num + " = " + product);
        }
	}

}
