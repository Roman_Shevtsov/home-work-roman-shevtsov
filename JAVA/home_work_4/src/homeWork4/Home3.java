//Выведите на экран прямоугольник из *. Причем высота и ширина прямоугольника вводятся с
//клавиатуры. Например ниже представлен прямоугольник с высотой 4 и шириной 5.
//*****
//*   *
//*   *
//*****

package homeWork4;

import java.util.Scanner;

public class Home3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);

        System.out.print("Введіть висоту прямокутника: ");
        int height = scanner.nextInt();

        System.out.print("Введіть ширину прямокутника: ");
        int width = scanner.nextInt();

        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++) {
                if (i == 1 || i == height || j == 1 || j == width) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
	}

}
