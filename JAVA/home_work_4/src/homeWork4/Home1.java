//Вычислить с помощью цикла факториал числа - n введенного с клавиатуры (4<n<16). Факториал
//числа это произведение всех чисел от этого числа до 1. Например 5!=5*4*3*2*1=120

package homeWork4;

import java.util.Scanner;

public class Home1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
        System.out.print("Введіть число (4 < n < 16): ");
        int n = scanner.nextInt();

        if (n <= 4 || n >= 16) {
            System.out.println("Невірне введення числа");
        } else {
            long factorial = 1;
            for (int i = n; i >= 1; i--) {
                factorial *= i;
            }
            System.out.println(n + " = " + factorial);
        }
	}

}
