/*
1). Разработайте функцию-конструктор, которaя будет создавать объект Human (человек). 
Создайте массив объектов и реализуйте функцию, которая будет сортировать элементы массива по значинию свойства Age по возрастанию или по убыванию.
*/


function Human(name, age) {
  this.name = name;
  this.age = age;
}
let humanArray = [];
humanArray.push(new Human("Славік", 23));
humanArray.push(new Human("Маряна", 18));
humanArray.push(new Human("Віталік", 20));
humanArray.push(new Human("Олег", 25));
humanArray.push(new Human("Сашко", 35));
humanArray.push(new Human("Олена", 40));
humanArray.push(new Human("Роман", 34));
humanArray.push(new Human("Наталка", 17));

document.write(`<hr>Сортування за зростанням:<br> `);

humanArray.sort((a, b) => a.age - b.age);
for (let item of humanArray) {
  document.write(` ${item.name} ${item.age} <br>`);

}

document.write(`<hr>Сортування за спаданням:<br>`);

humanArray.sort((a, b) => b.age - a.age);
for (let item of humanArray) {
  document.write(` ${item.name} ${item.age} <br>`);
}