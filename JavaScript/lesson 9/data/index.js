let startB = document.getElementById("start"),
    stopB = document.getElementById("stop"),
    resetB = document.getElementById("reset"),
    background = document.querySelector(".container-stopwatch");

let hoursElem = document.getElementById("hoursElem"),
    minutesElem = document.getElementById("minutesElem"),
    secondsElem = document.getElementById("secondsElem");

let intervalHandler,
    sec = 00,
    mins = 00,
    hrs = 00;

let counter = () => {
    sec++;
    secondsElem.textContent = sec;
    if (sec === 60) {
        mins++;
        sec = 00;
        minutesElem.textContent = mins;
    } else if (mins === 60) {
        hrs++;
        mins = 00;
        hoursElem.textContent = hrs;
    }
}

startB.addEventListener("click", () => {
    intervalHandler = setInterval(counter, 1000);
    background.classList.remove("black");
    background.classList.add("green");
})

stopB.addEventListener("click", () => {
    clearInterval(intervalHandler);
    background.classList.remove("green");
    background.classList.add("red");
})

resetB.addEventListener("click", () => {
    clearInterval(intervalHandler);
    sec = 00;
    mins = 00;
    hrs = 00;
    secondsElem.textContent = "00";
    minutesElem.textContent = "00";
    hoursElem.textContent = "00";
    background.classList.remove("red");
    background.classList.add("silver");
})