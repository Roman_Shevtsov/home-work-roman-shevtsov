/*Створи клас, який буде створювати користувачів з ім'ям та прізвищем. 
Додати до класу метод для виведення імені та прізвища*/
class User {
    constructor(name, surname) {
        this.name = name;
        this.surname = surname;
    }
    onDisplay() {
        return `User: ${this.name}, ${this.surname}`;
    }
}
document.querySelector('button').addEventListener('click', () => {
    let user = new User(
        document.getElementById('name').value,
        document.getElementById('surname').value
    );
    let output = document.querySelector('.output');
    output.innerText = (user.onDisplay());
})

/*Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний*/
let liBlue = document.querySelector('.first');
liBlue.style.backgroundColor = 'blue';
let liRed = document.querySelector('.third');
liRed.style.backgroundColor = 'red';

/*Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки*/
let div = document.createElement('div');
let span = document.createElement('span');
document.body.append(span);
document.body.append(div);

div.style.border = '5px dotted red';
div.style.height = '400px';
div.style.width = '600px';

div.addEventListener('mouseover', (event) => {
    span.innerText = `x: ${event.x}, y: ${event.y}`;
    span.style.fontSize = '20px';
});
div.addEventListener('mouseout', () => {
    span.innerText = '';
})

/*Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута*/
let btn1 = document.querySelector('.one'),
    btn2 = document.querySelector('.two'),
    btn3 = document.querySelector('.three'),
    btn4 = document.querySelector('.four');

let spanBtn = document.querySelector('.spanBtn');

btn1.addEventListener('click', () => {
    spanBtn.innerText = 'Button one is clicked';
})

btn2.addEventListener('click', () => {
    spanBtn.innerText = 'Button two is clicked';
})
btn3.addEventListener('click', () => {
    spanBtn.innerText = 'Button three is clicked';
})
btn4.addEventListener('click', () => {
    spanBtn.innerText = 'Button four is clicked';
})

/*Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці*/

let divPosition = document.createElement('div');
document.body.append(divPosition);

divPosition.style.width = '100px';
divPosition.style.height = '100px';
divPosition.style.background = 'black';
divPosition.style.position = 'relative';

divPosition.addEventListener('mouseover', () => {
    divPosition.style.left = `${Math.floor(Math.random() * 200)}px`;
    divPosition.style.top = `${Math.floor(Math.random() * 200)}px`;
});

divPosition.addEventListener('mouseleave', () => {
    divPosition.style.left = '';
    divPosition.style.top = '';
});

/*Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body*/
let inputColor = document.createElement('input');
document.body.append(inputColor);

inputColor.setAttribute('type', 'color');
inputColor.addEventListener('input', (event) => {
    document.style.background = event.target.value;
})

/*Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль*/
let inputLogin = document.createElement("input");
document.body.append(inputLogin);

inputLogin.setAttribute('type', 'text');
inputLogin.addEventListener('input', (event) => {
    console.log(event.target.value);
})

/*Створіть поле для введення даних у полі введення даних виведіть текст під полем*/
let inputOut = document.createElement("input");
let spanOut = document.createElement("span");
document.body.append(inputOut);
document.body.after(spanOut);

inputOut.setAttribute('type', 'text');
inputOut.addEventListener('input', (event) => {
    spanOut.innerText = event.target.value;
});