
let a = '';
let b = '';
let sign = '';
let memVal = '';
let memSign = document.querySelector('span');
let complete = false;
let check = false;

let digital = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'];
let action = ['-', '+', '*', '/', '='];
let out = document.querySelector('.display>input');


function clear() {
    a = '';
    b = '';
    sign = '';
    complete = false;
    out.value = 0;
}

function memorize() {
    memVal = out.value;
    memSign.textContent = 'm';
}

function recall() {
    if (check) {
        memVal = '';
        memSign.textContent = '';
        check = false;
    } else {
        a = memVal;
        out.value = memVal;
        check = true;
    }
}

document.querySelector('.keys').addEventListener('click', (e) => {
    if (e.target.value === 'C') {
        clear();
        return;
    }
    if (e.target.value === 'm-' || e.target.value === 'm+') {
        memorize();
        return
    }
    if (e.target.value === 'mrc') {
        recall();
        return
    }

    out.value = '';
    let key = e.target.value;

    if (digital.includes(key)) {
        if (b === '' && sign === '') {
            a += key;
            out.value = a;
        }
        else if (a !== '' && b !== '' && complete) {
            b = key;
            complete = false;
            out.value = b;
        }
        else {
            b += key;
            out.value = b;
        }
        return;
    }

    if (action.includes(key)) {
        if (sign !== '' && !complete) {
            switch (sign) {
                case "+":
                    a = (+a) + (+b);
                    break;
                case "-":
                    a = a - b;
                    break;
                case "*":
                    a = a * b;
                    break;
                case "/":
                    if (b === '0') {
                        out.value = 'error';
                        a = '';
                        b = '';
                        sign = '';
                        return;
                    }
                    a = a / b;
                    break;
            }
            complete = true;
            out.value = a;
        } else {
            sign = key;
            return;
        }
    }
})