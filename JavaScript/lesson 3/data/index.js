document.write("<hr>Домашня робота: <br><br>");
document.write("Створіть масив styles з елементами «Джаз» і «Блюз»<br>");
let styles = ['«Джаз»', '«Блюз»'];
document.write("Результат: <br><p> " + styles + "</p><br>");
//

document.write("Добавте «Рок-н-ролл» в кінець<br>");
styles.push('«Рок-н-ролл»');
document.write("Результат: <br><p> " + styles + "</p><br>");
//

document.write("Замініть значення середнього елементу на «Класика». Ваш код для пошуку значення в середині повинен працювати з масивом любої довжини<br>");
//styles.splice(1, 1, "«Класика»");


const clas = "«Класика»";
let stylesCenter = Math.floor(styles.length / 2);
  if (Math.floor(styles.length % 2) !== 0) {
  styles[stylesCenter] = clas;
  } else {
  styles.splice(stylesCenter, 0, clas);
  }
document.write("Результат: <br><p> " + styles + "</p><br>");
//

document.write("Видаліть перший елемент масиву та виведіть його на екран<br>");
let removed = styles.splice(0, 1);
document.write("Результат: <br><p> " + removed + "</p>");
document.write("Решта масиву: <br><p> " + styles + "</p><br>");
//

document.write(" Вставте «Реп» та «Реггі» на початок масиву<br>");
styles.unshift('«Реп»', '«Реггі»');
document.write("Результат: <br><p> " + styles + "</p><br><hr>");
//

document.write(" <hr>");
document.write("1. Дані два масива: ['a', 'b', 'c'] і [1, 2, 3]. Об'єднайте їх разом. <br>");
let arr1 = ['a', 'b', 'c'];
let arr2 = [1, 2, 3];
let arr3 = arr1.concat(arr2);
document.write("Результат: <br><p> " + arr3 + "</p><br><hr>");
//

document.write("2. Дано масив ['a', 'b', 'c']. Добавте йому у кінець елементи 1, 2, 3. <br>");
arr1.push(1, 2, 3);
document.write("Результат: <br><p> " + arr1 + "</p><br><hr>");
//

document.write("3. Дано масив [1, 2, 3]. Зробіть з нього масив [3, 2, 1].<br>");
arr2.reverse();
document.write("Результат: <br><p> " + arr2 + "</p><br><hr>");
//

document.write("4. Дано масив [1, 2, 3]. Добавте йому у кінець елементи 4, 5, 6. <br>");
arr2.reverse();
arr2.push(4, 5, 6);
document.write("Результат: <br><p> " + arr2 + "</p><br><hr>");
//

document.write("5. Дано масив [1, 2, 3]. Добавте йому на початок елементи 4, 5, 6. <br>");
arr2.splice(3, 3);
arr2.splice(0, 0, 4, 5, 6.);
document.write("Результат: <br><p> " + arr2 + "</p><br><hr>");
//

document.write("6. Дано масив ['js', 'css', 'jq']. Виведіть на екран перший елемент <br>");
let arr4 = ['js', 'css', 'jq'];
document.write("Результат: <br><p> " + arr4[0] + "</p><br><hr>");
//

document.write("7. Дано масив [1, 2, 3, 4, 5]. За допомогою метода slice запишіть в новий масив елементи [1, 2, 3].<br>");
arr2.splice(0, 3);
arr2.push(4, 5);
arr5 = arr2.slice(0, 3);
document.write("Результат: <br><p> " + arr5 + "</p><br><hr>");
//

document.write("8. Дано масив [1, 2, 3, 4, 5]. За допомогою метода splice перетворіть масив у [1, 4, 5]. <br>");
arr2.splice(1, 2);
document.write("Результат: <br><p> " + arr2 + "</p><br><hr>");
//

document.write("9. Дано масив [1, 2, 3, 4, 5]. За допомогою метода splice перетворіть масив у [1, 2, 10, 3, 4, 5]. <br>");
arr2.splice(1, 0, 2, 3);
arr2.splice(2, 0, 10);
document.write("Результат: <br><p> " + arr2 + "</p><br><hr>");
//

document.write("10. Дано масив [3, 4, 1, 2, 7]. відсортуйте його. <br>");
let arr6 = [3, 4, 1, 2, 7];
arr6.sort();
document.write("Результат: <br><p> " + arr6 + "</p><br><hr>");
//

document.write("11. Дано масив з елементами 'Hello, ', 'world' і '!'. Необхідно вивести на екран фразу 'Hello, world!'. <br>");
let arr7 = ['Hello, ', 'world', '!'];
document.write("Результат: <br><p> " + arr7.join(' ') + "</p><br><hr>");
//

document.write("12. В даному масиві ['Hello, ', 'world', '!']. Необхідно написати у нульовий елемент цього масиву слово 'Bye, ' (тобто замість слова 'Hello, ' буде 'Bye, '). <br>");
arr7.splice(0, 1, 'Bye, ');
document.write("Результат: <br><p>" + arr7.join(' ') + "</p><br><hr>");
//

document.write("13. Створіть масив arr із елементами 1, 2, 3, 4, 5 двома різними способами. <br>");
arr8 = [1, 2, 3, 4, 5];
arr9 = new Array(1, 2, 3, 4, 5);
document.write("Результат: <br><p>масив 1 - " + arr8 + "</p><br><p> масив 2 - " + arr9 + "</p><br><hr>");
//

document.write("15. Створіть масив arr = ['a', 'b', 'c', 'd'] і з його допомогою виведіть на екран рядок 'a+b, c+d'. <br>");
arr1.splice(3, 5);
arr1.push('d');
document.write("Результат: <br><p>" + arr1[0] + "+" + arr1[1] + ", " + arr1[2] + "+" + arr1[3] + "</p><br><hr>");
//

document.write(" 16. Попросіть у користувача ввести кількість елементів масиву. Виходячи з данних котрі ввів користувач створіть масив на ту кількість елементів котрі ввів користувач. В кожному індексі масиву зберігайте число котре буде показувати номер елементу масиву.<br>");
let userArr = parseInt(prompt("Введіть довжину масиву"));
let userArr1 = [];
for (let i = 0; i < userArr; i++) {
  userArr1[i] = i;
}
document.write("Результат: <br><p>" + userArr1.join(", ") + "</p><br><hr>");
//

document.write("17. Зробіть так, щоб з масиву котрий ви створили вище вивелись всі непарні числа у параграфі, а парні числа у спані з червоним фоном. <br>");
document.write("Результат: " + "<br>");
userArr1.forEach(function (item) {
  if (item % 2 !== 0) {
    document.write("<p>Число " + item + " в параграфі</p>");
  } else if (item % 2 !== 1) {
    document.write("<span>Число " + item + " в спані</span>");
  }
});
document.write("<br><hr>");
//

document.write("18. Напишіть код, котрий перетворює і об'єднує всі елементи масиву в одне строкове значення. Елементи масиву будуть розділені комою. <br>");
var vegetables = ['Капуста', 'Ріпка', 'Редиска', 'Морква'];
let str1 = vegetables.join(", ");
document.write("Результат: <br><p>" + str1 + "<p><hr>");
