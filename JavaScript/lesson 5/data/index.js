/*Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". Создать в объекте вложенный объект - "Приложение". Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". Создать методы для заполнения и отображения документа.
*/

let content = {
    head: "", body: "", footer: "", data: "",
    print:  function (name) {
         document.write(`<p>title ${name} - ${this.head}`);
         document.write(`<p>body ${name} - ${this.body}`);
         document.write(`<p>footer ${name} - ${this.footer}`);
         document.write(`<p>data ${name} - ${this.data}`);
     }
};

let myDocument = content;
myDocument.application = content;
myDocument.append = function () {
     myDocument.head = prompt("Введіть заголовок документа!");
     myDocument.application.head = myDocument.head;
     myDocument.body = prompt("Введіть тіло документа!");
     myDocument.application.body = myDocument.body;
     myDocument.footer = prompt("Введіть футтер документа!");
     myDocument.application.footer = myDocument.footer;
     myDocument.data = prompt("Введіть дату документа!");
     myDocument.application.data = myDocument.data;
 };

 myDocument.append();
 myDocument.print("документа");
 myDocument.application.print("програми");
