package homeWork1;

public class AnimalApp {
    public static void main(String[] args) {
        Cat cat = new Cat("Корм", "Серый", 4, "Мурзик");
        Dog dog = new Dog("Мясо", "Черный", 30, "Бобик");

        Veterinarian veterinarian = new Veterinarian("Олег");

        System.out.println(cat);
        System.out.println(cat.getVoice());


        System.out.println();

        System.out.println(dog);
        System.out.println(dog.getVoice());

        System.out.println();

        veterinarian.treatment(cat);
        veterinarian.treatment(dog);

        System.out.println();

        System.out.println("Кіт їсть: " + cat.getRation());
        System.out.println("Собака їсть: " + dog.getRation());

        System.out.println("Кіт спить на подушці");
        System.out.println("Собака спить у будці");
    }
}