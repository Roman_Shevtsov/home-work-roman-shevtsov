package homeWork1;

public class Cat extends Animal {
    private String name;

    public Cat(String ration, String color, int weight, String name) {
        super(ration, color, weight);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getVoice() {
        return "Мяу!";
    }

    @Override
    public void eat() {
        System.out.println("Кітка їсть");
    }

    @Override
    public void sleep() {
        System.out.println("Кітка спить");
    }

    @Override
    public String toString() {
        return "Cat [name=" + name + ", ration=" + ration + ", color=" + color + ", weight=" + weight + "]";
    }
}