package homeWorkUp;

public class UpDownTransformer extends TextTransformer {
    @Override
    public String transform(String text) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            result.append((i % 2 == 0) ? Character.toUpperCase(c) : Character.toLowerCase(c));
        }
        return result.toString();
    }
}