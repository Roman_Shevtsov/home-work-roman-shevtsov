package homeWorkUp;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TextSaver {
    private TextTransformer transformer;
    private File file;

    public TextSaver(TextTransformer transformer, File file) {
        this.transformer = transformer;
        this.file = file;
    }

    public void saveTextToFile(String text) {
        String transformedText = transformer.transform(text);
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(transformedText);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}