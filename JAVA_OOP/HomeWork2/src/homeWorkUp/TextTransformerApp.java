package homeWorkUp;

import java.io.File;

public class TextTransformerApp {
    public static void main(String[] args) {
        TextTransformer transformer1 = new TextTransformer();
        InverseTransformer transformer2 = new InverseTransformer();
        UpDownTransformer transformer3 = new UpDownTransformer();

        TextSaver textSaver1 = new TextSaver(transformer1, new File("output1.txt"));
        TextSaver textSaver2 = new TextSaver(transformer2, new File("output2.txt"));
        TextSaver textSaver3 = new TextSaver(transformer3, new File("output3.txt"));

        String inputText = "Доброго вечора ми з України";

        textSaver1.saveTextToFile(inputText);
        textSaver2.saveTextToFile(inputText);
        textSaver3.saveTextToFile(inputText);
    }
}