package classWork3;

import java.io.File;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class work1 {

	public static void main(String[] args) {
		File file = new File("price.txt");
	    Integer price = null;
	    try {
	      Scanner sc = new Scanner(file);
	      price = sc.nextInt();
	    } catch (IOException e) {
	      System.out.println("File not found");
	    } catch (InputMismatchException e) {
	      System.out.println("Error file format");
	    }
	    System.out.println("price = " + price);
	}

}
