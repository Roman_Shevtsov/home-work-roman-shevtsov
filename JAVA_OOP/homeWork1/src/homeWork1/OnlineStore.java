package homeWork1;

public class OnlineStore {
    public static void main(String[] args) {
        Laptop laptop1 = new Laptop(1500.0, "Apple", "MacBook Pro", 13.3, "macOS");
        Laptop laptop2 = new Laptop(1100.0, "Dell", "XPS 13", 13.4, "Windows 11");
        Laptop laptop3 = new Laptop(1200.0, "HP", "Spectre x360", 13.3, "Windows 11");
        Laptop laptop4 = new Laptop(2700.0, "Lenovo", "ThinkPad X1 Carbon", 14.0, "Windows 11");
        Laptop laptop5 = new Laptop(2200.0, "ASUS", "ROG Zephyrus G14", 14.0, "Windows 11");

        Smartphone smartphone1 = new Smartphone(1000.0, "Apple", "iPhone 13", 6.1, "iOS 15");
        Smartphone smartphone2 = new Smartphone(1100.0, "Samsung", "Galaxy S21", 6.2, "Android 12");
        Smartphone smartphone3 = new Smartphone(700.0, "Google", "Pixel 6", 6.4, "Android 12");
        Smartphone smartphone4 = new Smartphone(900.0, "OnePlus", "9 Pro", 6.7, "Android 12");
        Smartphone smartphone5 = new Smartphone(800.0, "Xiaomi", "Mi 11", 6.81, "Android 12");

        // Виведення інформації про всі ноутбуки та смартфони
        System.out.println(laptop1);
        System.out.println(laptop2);
        System.out.println(laptop3);
        System.out.println(laptop4);
        System.out.println(laptop5);

        System.out.println(smartphone1);
        System.out.println(smartphone2);
        System.out.println(smartphone3);
        System.out.println(smartphone4);
        System.out.println(smartphone5);
    }
}