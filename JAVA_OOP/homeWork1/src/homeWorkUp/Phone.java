package homeWorkUp;

public class Phone {
    private String number;
    private String brand;
    private String operatingSystem;
    private Network network;

    public Phone(String number, String brand, String operatingSystem) {
        this.number = number;
        this.brand = brand;
        this.operatingSystem = operatingSystem;
    }

    public void registerInNetwork(Network network) {
        this.network = network;
        network.registerPhone(this);
    }

    public void call(String toNumber) {
        if (network == null) {
            System.out.println("Телефон не зареєстрований в мережі!");
            return;
        }
        if (number.equals(toNumber)) {
            System.out.println("Дзвінок самому собі неможливий!");
            return;
        }
        Phone toPhone = network.getPhoneByNumber(toNumber);
        if (toPhone != null) {
            toPhone.receiveCall(number);
        } else {
            System.out.println("Телефон за номером " + toNumber + " не зареєстрований в мережі!");
        }
    }

    public void receiveCall(String fromNumber) {
        System.out.println("Вам звонить номер: " + fromNumber);
    }

    public String getNumber() {
        return number;
    }
}