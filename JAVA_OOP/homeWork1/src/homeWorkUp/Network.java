package homeWorkUp;

import java.util.HashMap;
import java.util.Map;

public class Network {
    private Map<String, Phone> phones = new HashMap<>();

    public void registerPhone(Phone phone) {
        phones.put(phone.getNumber(), phone);
    }

    public Phone getPhoneByNumber(String number) {
        return phones.get(number);
    }
}