package homeWorkUp;

public class PhoneApp {
    public static void main(String[] args) {
        Network network = new Network();

        Phone phone1 = new Phone("123", "Apple", "iOS");
        Phone phone2 = new Phone("456", "Samsung", "Android");
        Phone phone3 = new Phone("789", "Google", "Android");

        phone1.registerInNetwork(network);
        phone2.registerInNetwork(network);
        phone3.registerInNetwork(network);

        phone1.call("456"); 
        phone1.call("789"); 
        phone2.call("123");
        phone2.call("000"); 
        phone1.call("123"); 
    }
}