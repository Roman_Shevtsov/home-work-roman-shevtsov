package homeWork2;

public class TriangleApp {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(3, 4, 5);
        Triangle triangle2 = new Triangle(5, 12, 13);
        Triangle triangle3 = new Triangle(7, 24, 25);

        triangle1.displayArea();
        triangle2.displayArea();
        triangle3.displayArea();
    }
}